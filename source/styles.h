// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __STYLES_H
#define __STYLES_H


#include <clxclient.h>


enum
{
    C_MAIN_BG, C_MAIN_FG,
    C_MAIN_LS, C_MAIN_DS,
    C_DISP_BG, C_DISP_FG,
    C_BUTT_B0, C_BUTT_F0,
    C_BUTT_B1, C_BUTT_F1,
    C_BUTT_B2, C_BUTT_F2,
    NXFTCOLORS
};

enum
{
    F_TEXT, F_BUTT, F_DISP,
    NXFTFONTS
};


extern void styles_init (X_display *disp, X_resman *xrm);
extern void styles_fini (X_display *disp);

extern XftColor  *XftColors [NXFTCOLORS];
extern XftFont   *XftFonts [NXFTFONTS];

extern X_textln_style tstyle1;
extern X_textln_style tstyle2;
extern X_button_style bstyle1;


#endif
