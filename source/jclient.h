// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <jack/jack.h>
#include "global.h"
#include "acfilter.h"
#include "lpeq20filter.h"
#include "itu468filter.h"
#include "vumdetect.h"
#include "rmsdetect.h"
#include "itu468detect.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv);
    ~Jclient (void);
  
    const char *jname (void) const { return _jname; }
    float get_value (void) const { return _val; }

    void set_input (int input);
    void set_dcfilt (int dcfilt);
    void set_filter (int filter);
    void set_detect (int detect);
    void set_slower (bool slower);

private:

    void  init_jack (const char *jname, const char *jserv);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_process (int frames);

    virtual void thr_main (void) {}

    jack_client_t  *_jack_client;
    jack_port_t    *_jack_testip;
    jack_port_t    *_inpports [8];
    jack_port_t    *_outports [1];
    const char     *_jname;
    unsigned int    _fsamp;
    int             _state;
    int             _input;
    int             _dcfilt;
    int             _filter;
    int             _detect;
    bool            _slower;
    float           _dcw;
    float           _dcz;
    float           _val;

    ACfilter        _acwfilter;
    LPeq20filter    _lpefilter;
    Itu468filter    _itufilter;
    VUMdetect       _vumdetect;
    RMSdetect       _rmsdetect;
    Itu468detect    _itudetect;
  
    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
