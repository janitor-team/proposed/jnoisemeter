// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GLOBAL_H
#define __GLOBAL_H


#define  PROGNAME       "jnoisemeter"

#define  EV_X11         16
#define  EV_EXIT        31

#define  DCF_OFF        0
#define  DCF_5HZ        1

#define  FIL_NONE       0
#define  FIL_20KHZ      1
#define  FIL_IECA       2
#define  FIL_IECC       3
#define  FIL_ITU1       4
#define  FIL_ITU2       5

#define  DET_ITU        0
#define  DET_RMS        1
#define  DET_VUM        2

#endif
